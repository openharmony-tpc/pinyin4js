# pinyin4js

## Introduction

pinyin4js is an open-source JavaScript library that has been adapted to OpenHarmony for converting Chinese characters to Pinyin. It has the following features:

* Zero dependency

* Flexible import and packaging of word dictionaries  
  You can adjust the dictionary. For details, see **src/dict**. All resource calls are encapsulated by PinyinResource. You can modify resources before packaging them.

* Accurate and complete font library 
  For the 20903 Chinese characters in the Unicode 4E00-9FA5 range and 3007 (〇), pinyin4js can convert all Chinese characters except 46 heterogeneity characters that have no standard pinyin.

* Fast pinyin conversion 
  According to the test result, pinyin4js takes about 110 ms to process 20902 Chinese characters from 4E00 to 9FA5.

* Multi-pinyin output 
  Supports multiple pinyin output formats: with or without phonetic symbols, with phonetic symbols represented by digits, and with the initial letter of the pinyin.

* Multi-tone recognition 
  Supports recognition of common multi-tone characters, including phrases, idioms, and place names.

* Simplified and Traditional Chinese conversion

* Adding a user-defined dictionary 
  Supports the adding of user-defined dictionaries.

## How to Install
```
ohpm install @ohos/pinyin4js
```

## How to Use

```javascript
    import {pinyin4js} from '@ohos/pinyin4js';

    // more detail methods in test
    // WITH_TONE_NUMBER indicates that tones are represented by numbers, WITHOUT_TONE indicates pinyin without tones, and WITH_TONE_MARK indicates pinyin with tones.
    // output: xià#mén#nǐ#hǎo#dà#shà#xià#mén
    console.info("00771-" + pinyin4js.convertToPinyinString ('jiaodui', '#', pinyin4js.WITH_TONE_MARK));
    console.info("00771-" + pinyin4js.convertToPinyinString ('xuexiao', '#', pinyin4js.WITH_TONE_MARK));
    console.info("00772-" + pinyin4js.convertToPinyinString ('Xiamennihaodaxiaxiamen', '#', pinyin4js.WITHOUT_TONE));
    
    // Initial letter style
    // output: xmnhdsxm
    console.info("00773-" + pinyin4js.convertToPinyinString ('Xiamennihaodaxiaxiamen',", pinyin4js.FIRST_LETTER));
    // or
    console.info("00774-" + pinyin4js.getShortPinyin ('Xiamennihaodaxiaxiamen');
    
    // Traditional Chinese to simplified Chinese
    console.info("00775-" + pinyin4js.convertToSimplifiedChinese ('歲月時光'));
    // Simplified Chinese to Traditional Chinese
    console.info("00776-" + pinyin4js.convertToTraditionalChinese ('岁月时光');
    
```

## Available APIs

|                API                |              Description              |
|:----------------------------------:|:-------------------------------:|
|      convertToPinyinString()       |         Converts a string into pinyin of the corresponding format.         |
|    convertToSimplifiedChinese()    |            Converts traditional Chinese characters to simplified Chinese characters.            |
|   convertToTraditionalChinese()    |            Convert Simplified to Traditional Chinese.            |
|          getShortPinyin()          |            Obtains the initial letter of pinyin.            |
|          hasMultiPinyin()          |          Checks whether a Chinese character is a multi-tone character.          |
|         containsChinese()          |             Checks whether Chinese characters are contained.             |
|            isChinese()             |             Checks whether the character is a Chinese character.            |

## Constraints

This project has been verified in the following version:

DevEco Studio: DevEco Studio:4.0 (4.0.3.512), SDK:API10 (4.0.10.9)

## About obfuscation
- Code obfuscation, please see[Code Obfuscation](https://docs.openharmony.cn/pages/v5.0/zh-cn/application-dev/arkts-utils/source-obfuscation.md)
- If you want the pinyin4js library not to be obfuscated during code obfuscation, you need to add corresponding exclusion rules in the obfuscation rule configuration file obfuscation-rules.txt：
```
-keep
./oh_modules/@ohos/pinyin4js
```

## How to Contribute
If you find any problem when using the project, submit an [Issue](https://gitee.com/openharmony-tpc/pinyin4js/issues) or a [PR](https://gitee.com/openharmony-tpc/pinyin4js/pulls) to us.

## License
This project is licensed under [MIT License](https://gitee.com/openharmony-tpc/pinyin4js/blob/master/LICENSE).
